<?php
/**
 * Plugin Name: File Version Cache Busting
 * Plugin URI: https://www.micronics.mx
 * Description: Bust cache based on file modification date.
 * Version: 0.2
 * Text Domain: tbare-wordpress-plugin-demo
 * Author: Juan Salinas
 * Author URI: https://jmsbportfolio.herokuapp.com/
 */

function fvcb_add_settings_page() {
    add_options_page( 'File Version Cache Busting Settings', 'File Version Cache Busting', 'manage_options', 'file-version-cache-busting', 'fvcb_render_plugin_settings_page' );
}
add_action( 'admin_menu', 'fvcb_add_settings_page' );

function fvcb_render_plugin_settings_page() {
    ?>
    <h2>Plugin Settings</h2>
    <form action="options.php" method="post">
        <?php 
        settings_fields( 'fvcb-plugin-options' );
        do_settings_sections( 'file-version-cache-busting' ); ?>
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    </form>
    <?php
}

function fvcb_register_settings() {
    register_setting( 'fvcb-plugin-options', 'fvcb-plugin-options', 'fvcb_options_validate' );
    add_settings_section( 'path_settings', 'Path Settings', 'fvcb_plugin_section_text', 'file-version-cache-busting' );
    add_settings_field( 'fvcb_plugin_root_path', 'Root path', 'fvcb_plugin_setting_root_path', 'file-version-cache-busting', 'path_settings' );
}
add_action( 'admin_init', 'fvcb_register_settings' );

function fvcb_options_validate( $input ) {
    $newinput['api_key'] = trim( $input['api_key'] );
    if ( ! preg_match( '/^[a-z0-9]{32}$/i', $newinput['api_key'] ) ) {
        $newinput['api_key'] = '';
    }

    return $newinput;
}

function fvcb_plugin_section_text() {
    echo '<p>Here you may register your settings!</p>';
}

function fvcb_plugin_setting_root_path() {
    $options = get_option( 'fvcb_plugin_options' );
    echo "<input id='fvcb_plugin_setting_root_path' name='fvcb_plugin_setting_root_path[root_path]' type='text' value='".esc_attr($options['root_path'])."' />";
}

function assetVersionShortcode($atts) {
    $baseDir = ABSPATH;
    $versionStr = "ver";
    $src = $atts["src"];
    $path = $atts["path"];
    $filename = basename($src);
    $ext = pathinfo($filename, PATHINFO_EXTENSION); 
    $type = "";

    if(isset($path))
        $fileToVersion = $baseDir . $path;
    else
        $fileToVersion = $src;

    $version = filemtime($fileToVersion);
    $versionedSrc = $src. "?$versionStr=$version";

    if(isset($atts["type"]))
    {
        $type = $atts["type"];
    }
    else
    {
        switch($ext)
        {
            case "js":
                $type = "script";
            break;
            case "css":
                $type = "style";
            break;
        }
    }

    switch($type)
    {
        case "script":
            return "<script type='text/javascript' src='$versionedSrc'></script>";
            break;
        case "style":
            return "<link type='text/css' rel='stylesheet' href='$versionedSrc' />";
            break;
    }
}
add_shortcode('asset_version_include', 'assetVersionShortcode');


