# wordpress-plugins

Git repository for Wordpress plugins developed by Micronics.mx

Plugins:

- **File Version Cache Busting:** Implements a shortcode for quickly adding versioning to your static files, by adding the last modified date as a querystring parameter in your tags. It's main advantage is letting the developer easily add resources to an already existing template (by adding HTML block an then writing the shortcode), without having to modify it directly. Currently, it only works with CSS and JS files. More functionalities will be added later.

    Quick example:

    - path = there goes the file URL, like the one you'd put the script/link tag (can be relative)
    - src = there goes the file path in the server (can be relative)

    `[asset_version_include path="/js/myScript.js" src="../js/myScript.js"]`

